# locust-test1: 
### A simple locust.io testing configuration that uses Docker to orchestrate a network of a single Master and multiple Slaves (5 by default)

## Prerequisites
* Functional Docker Installation with Docker-Compose - Recommend Docker Community Edition for Mac (Docker CE) - https://store.docker.com/editions/community/docker-ce-desktop-mac
* A website that you have permission to test! (Especially if you're going to get pushy)

## TL;DR - Running a test from local docker with 1 Master 1 Slave (not ideal)

* clone the project
* cd to project root
* set the env variable for the host you want to test (fqdn) - `export TARGET_HOST=https://cnn.com`
* `docker-compose up` (optionally add -d to background it)
* Connect to http://localhost:8089 to start and observe tests


## Doing more

Edit locustfile.py and add your own tests, conditions etc.

If you're going to run on a cluster you may benefit from building and pushing an image to a common registry

`docker build -t <username>/<image>:<tag> .` - don't miss the period, it tells the docker build process the context for the build.
replace <username>, <image> and <tag> with appropriate values. 
If you have a dockerhub account you can use that info and push containers up to them. Be careful, it's public by default.

If you named your image appropriately (repo/image:tag) then you should be able to push it to a registry. 

Replace the `build: .` with `image: <registry>/<image>:<tag>` in the docker-compose.yml file with the image you've built to run/deploy the app with your newly created image.

If you use `docker-compose up` or `docker stack deploy` you can set an environment variable TARGET_HOST and the locust construct will run tests against that host.

Example:
`export TARGET_HOST=https://digimercs.net`
`docker stack deploy -c docker-compose.yml locust`

Using this project the above two commands should deploy a set of 6 cans:
1x locust-master with the web UI running on port 8089 
5x locust-clients

Connect to locust-master:8089 to start your first test. 

