FROM python:3.5-slim
RUN pip install --upgrade pip && pip install locustio
WORKDIR /opt/locust/
COPY *.py /opt/locust/
